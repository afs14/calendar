﻿using Volo.Abp.Modularity;

namespace AFS.Calendar;

[DependsOn(
    typeof(CalendarApplicationModule),
    typeof(CalendarDomainTestModule)
    )]
public class CalendarApplicationTestModule : AbpModule
{

}
