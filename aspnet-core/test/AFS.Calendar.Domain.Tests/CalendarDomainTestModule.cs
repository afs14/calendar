﻿using AFS.Calendar.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace AFS.Calendar;

[DependsOn(
    typeof(CalendarEntityFrameworkCoreTestModule)
    )]
public class CalendarDomainTestModule : AbpModule
{

}
