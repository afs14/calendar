﻿using AFS.Calendar.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace AFS.Calendar.Permissions;

public class CalendarPermissionDefinitionProvider : PermissionDefinitionProvider
{
    public override void Define(IPermissionDefinitionContext context)
    {
        var myGroup = context.AddGroup(CalendarPermissions.GroupName);
        //Define your own permissions here. Example:
        //myGroup.AddPermission(CalendarPermissions.MyPermission1, L("Permission:MyPermission1"));
    }

    private static LocalizableString L(string name)
    {
        return LocalizableString.Create<CalendarResource>(name);
    }
}
