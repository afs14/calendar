﻿using AFS.Calendar.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace AFS.Calendar.DbMigrator;

[DependsOn(
    typeof(AbpAutofacModule),
    typeof(CalendarEntityFrameworkCoreModule),
    typeof(CalendarApplicationContractsModule)
    )]
public class CalendarDbMigratorModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpBackgroundJobOptions>(options => options.IsJobExecutionEnabled = false);
    }
}
