﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using AFS.Calendar.Data;
using Volo.Abp.DependencyInjection;

namespace AFS.Calendar.EntityFrameworkCore;

public class EntityFrameworkCoreCalendarDbSchemaMigrator
    : ICalendarDbSchemaMigrator, ITransientDependency
{
    private readonly IServiceProvider _serviceProvider;

    public EntityFrameworkCoreCalendarDbSchemaMigrator(
        IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public async Task MigrateAsync()
    {
        /* We intentionally resolving the CalendarDbContext
         * from IServiceProvider (instead of directly injecting it)
         * to properly get the connection string of the current tenant in the
         * current scope.
         */

        await _serviceProvider
            .GetRequiredService<CalendarDbContext>()
            .Database
            .MigrateAsync();
    }
}
