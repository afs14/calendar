﻿using Volo.Abp.Settings;

namespace AFS.Calendar.Settings;

public class CalendarSettingDefinitionProvider : SettingDefinitionProvider
{
    public override void Define(ISettingDefinitionContext context)
    {
        //Define your own settings here. Example:
        //context.Add(new SettingDefinition(CalendarSettings.MySetting1));
    }
}
