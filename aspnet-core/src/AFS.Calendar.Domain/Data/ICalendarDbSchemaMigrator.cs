﻿using System.Threading.Tasks;

namespace AFS.Calendar.Data;

public interface ICalendarDbSchemaMigrator
{
    Task MigrateAsync();
}
