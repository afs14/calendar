﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace AFS.Calendar.Data;

/* This is used if database provider does't define
 * ICalendarDbSchemaMigrator implementation.
 */
public class NullCalendarDbSchemaMigrator : ICalendarDbSchemaMigrator, ITransientDependency
{
    public Task MigrateAsync()
    {
        return Task.CompletedTask;
    }
}
