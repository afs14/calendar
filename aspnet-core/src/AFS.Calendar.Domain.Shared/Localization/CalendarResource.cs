﻿using Volo.Abp.Localization;

namespace AFS.Calendar.Localization;

[LocalizationResourceName("Calendar")]
public class CalendarResource
{

}
