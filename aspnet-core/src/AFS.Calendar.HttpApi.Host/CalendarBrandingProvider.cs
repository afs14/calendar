﻿using Volo.Abp.DependencyInjection;
using Volo.Abp.Ui.Branding;

namespace AFS.Calendar;

[Dependency(ReplaceServices = true)]
public class CalendarBrandingProvider : DefaultBrandingProvider
{
    public override string AppName => "Calendar";
}
