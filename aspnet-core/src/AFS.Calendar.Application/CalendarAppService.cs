﻿using System;
using System.Collections.Generic;
using System.Text;
using AFS.Calendar.Localization;
using Volo.Abp.Application.Services;

namespace AFS.Calendar;

/* Inherit your application services from this class.
 */
public abstract class CalendarAppService : ApplicationService
{
    protected CalendarAppService()
    {
        LocalizationResource = typeof(CalendarResource);
    }
}
