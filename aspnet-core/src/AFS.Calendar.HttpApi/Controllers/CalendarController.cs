﻿using AFS.Calendar.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace AFS.Calendar.Controllers;

/* Inherit your controllers from this class.
 */
public abstract class CalendarController : AbpControllerBase
{
    protected CalendarController()
    {
        LocalizationResource = typeof(CalendarResource);
    }
}
